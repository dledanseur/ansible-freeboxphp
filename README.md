# dledanseur.freeboxphp

This role install the Freebox Revolution API (sources: https://github.com/DjMomo/ClassePhpFreebox), which allow rebooting the Freebox, turning the Wifi on/off, etc.  

Optionally, this role can schedule a periodic reboot, via the following variables:  

```
freebox_periodic_reboot: false
freebox_periodic_reboot_weekday: '*'
freebox_periodic_reboot_minute: '0'
freebox_periodic_reboot_hour: '4'
freebox_periodic_reboot_month: '*'
```

It depends on the role dledanseur.php.    

Compatibility: Ubuntu > 16.04, Debian > 8
